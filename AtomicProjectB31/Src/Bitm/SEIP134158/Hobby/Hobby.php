<?php
namespace App\Bitm\SEIP134158\Hobby;
use App\Bitm\SEIP134158\Utility\Utility;
use PDO;
use App\Bitm\SEIP134158\Message\Message;

class Hobby
{
    public $id="";
    public $name="";
    public $hobby="";
    public $serverName="localhost";
    public $databaseName="atomicproject";
    public $user="root";
    public $pass="";
    public $conn;

    public function __construct()
    {
        try {
            # MySQL with PDO_MYSQL
            $this->conn = new PDO("mysql:host=$this->serverName;dbname=$this->databaseName", $this->user, $this->pass);
        }
        catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function setData($data=''){
        if(array_key_exists('name',$data) && !empty($data)){
            $this->name=$data['name'];
        }

        if(array_key_exists('hobby',$data) && !empty($data)){
            $this->hobby=$data['hobby'];
        }
        if(array_key_exists('id',$data) && !empty($data)){
            $this->id=$data['id'];
        }
        return $this;
    }
    public function store(){
        $query="INSERT INTO `hobby` (`name`, `hobby`) VALUES (:yName, :yHobby)";
        $stmt=$this->conn->prepare( $query);
        $result=$stmt->execute(array(':yName'=>$this->name,
            ':yHobby'=>$this->hobby
        ));
        if($result){
            //header('Location:index.php');
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> Data has been stored successfully.
</div>");
            Utility::redirect('index.php');
        }
        else{
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Some error has been occured
</div>");
            Utility::redirect('index.php');
        }

    }

    public function index(){
        $sqlquery="SELECT * FROM `hobby`";
        $stmt=$this->conn->query($sqlquery);
        $alldata=$stmt->fetchAll(PDO::FETCH_OBJ);
        return $alldata;
    }
    public function view(){
        $sqlquery="SELECT * FROM `hobby` WHERE `id`=:setid";
        $stmt=$this->conn->prepare($sqlquery);
        $stmt->execute(array(':setid'=>$this->id
        ));
        $singledata=$stmt->fetch(PDO::FETCH_OBJ);
        return $singledata;
    }
    public function update(){
        $query="UPDATE `hobby` SET `name` = `:getName` `hobby`=`:getHobby` WHERE `hobby`.`id` = :id";
        $stmt=$this->conn->prepare( $query);
        $result=$stmt->execute(array(':getName'=>$this->name, ':getHobby'=>$this->hobby, ':id'=>$this->id));
        return $result;
//        if($result){
//            //header('Location:index.php');
//            Message::message("<div class=\"alert alert-success\">
//  <strong>Success!</strong> Data has been updated successfully.
//</div>");
//            Utility::redirect('index.php');
//        }
//        else{
//            Message::message("<div class=\"alert alert-danger\">
//  <strong>Error!</strong> Some error has been occured
//</div>");
//            Utility::redirect('index.php');
//        }

    }

}