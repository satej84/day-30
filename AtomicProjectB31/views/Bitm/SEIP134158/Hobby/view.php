<?php
include_once('../../../../vendor/autoload.php');
use \App\Bitm\SEIP134158\Hobby\Hobby;
use App\Bitm\SEIP134158\Utility\Utility;

$obj= new Hobby();
$result=$obj->setData($_GET)->view();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Hobby</title>

</head>
<body>
    <table>

        <tr>

            <h2>Your Hobbies</h2>
            <ul>
                <li >ID: <?php echo $result->id ?></li>
                <li >Your Name: <?php echo $result->name ?></li>
                <li >Your Hobbies:<?php echo $result->hobby ?></li>
            </ul>
        </tr>
    </table>
</body>
</html>
