<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Atomic Project</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>

        <div class="container">
            <h2>Hobby</h2>
            <form action="store.php" method="post">
                <div class="form-group">
                    <label for="name">Your Name:</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter Your Name">
                </div>
                <div class="form-group">
                    <label>Your Hobby:</label>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="hobby[]" value="football" >Football
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="hobby[]"  value="cricket">Cricket
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="hobby[]" value="computer" >Computer
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="hobby[]"value="gardening"  >Gardening
                        </label>
                    </div>
                </div>


                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>

    </body>
</html>