<?php
session_start();
include_once('../../../../vendor/autoload.php');

use App\Bitm\SEIP134158\Hobby\Hobby;
use App\Bitm\SEIP134158\Message\Message;

$obj= new Hobby();
//$obj->setData();
//var_dump($all);
$allResult = $obj->index();

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>view</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div >
            <div id="message">
                <?php
                if(array_key_exists('success_message',$_SESSION) and !empty($_SESSION['success_message'])){
                    echo Message::message();
                }
                ?>
            </div>


            <a class="btn btn-primary" href="create.php" >Create Again</a>

            <h2>Your Hobbies List</h2>
            <table class="table table-bordered" >
                <thead>
                    <tr>

                        <th>SL</th>
                        <th>ID</th>
                        <th>Your Name</th>
                        <th>Your Hobbies</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sl=0;
                    foreach($allResult as $result){
                        $sl++?>
                        <tr>
                            <td><?php echo $sl?></td>
                            <td><?php echo $result->id ?></td>
                            <td><?php echo $result->name ?></td>
                            <td><?php echo $result->hobby ?></td>

                            <td>
                                <a href="view.php?id=<?php echo $result->id?>" class="btn btn-primary" >View</a>
                                <a href="edit.php" class="btn btn-success" >Edit</a>
                                <a href="delete.php" class="btn btn-danger" >Delete</a>
                            </td>
                        </tr>
                    <?php } ?>

                </tbody>
            </table>
        </div>
    <script>
        $("#message").show().delay(3000).fadeOut();
    </script>

    </body>
</html>
