<?php
include_once('../../../../vendor/autoload.php');
use App\Bitm\SEIP134158\Hobby\Hobby;
use App\Bitm\SEIP134158\Utility\Utility;

$hobby = new Hobby();
$all=$hobby->setData($_GET);

$singleData = $hobby->view();

$hobbyString =  $singleData['hobby'];
$hobbyArray = explode(',',$hobbyString);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Atomic Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Update Hobbies list</h2>
    <form action="update.php" method="post">
        <div class="form-group">
            <label for="name">Your Name:</label>
<!--            <input type="text" class="form-control" id="id" name="id"  value="--><?php //echo $hobbyString['id'] ?><!--">-->
            <input type="text" class="form-control" id="name" name="name" placeholder="Enter Your Name" value="<?php echo $singleData['name'] ?>">
        </div>
        <div class="form-group">
            <label>Your Hobby:</label>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="hobby[]" value="football" <?php
                    if(in_array('football',$hobbyArray)){
                        echo 'checked';
                    }
                    ?> >Football
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="hobby[]"  value="cricket"<?php
                    if(in_array('cricket',$hobbyArray)){
                        echo 'checked';
                    }
                    ?>
                    >Cricket
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="hobby[]" value="computer" <?php
                    if(in_array('computer',$hobbyArray)){
                        echo 'checked';
                    }
                    ?>>Computer
                </label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]"value="gardening" <?php
                    if(in_array('gardening',$hobbyArray)){
                        echo 'checked';
                    }
                    ?>  >Gardening</label>
            </div>
        </div>


        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

</body>
</html>