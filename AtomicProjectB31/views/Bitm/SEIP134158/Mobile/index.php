<?php
session_start();
//echo $_SERVER['DOCUMENT_ROOT'];
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."AtomicProjectB31".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");


use App\Bitm\SEIP134158\Mobile\Mobile;
use App\Bitm\SEIP134158\Message\Message;

$obj= new Mobile();
$allResult = $obj->index();
//App\Bitm\SEIP134158\Utility\Utility::dd($allResult);


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>


<div class="container">
    <div id="message">
        <?php
        if(array_key_exists('success_message',$_SESSION) and !empty($_SESSION['success_message'])){
            echo Message::message();
        }
        ?>
    </div>

    <a href="create.php" class="btn btn-primary" role="button">Create Again</a>



    <h2>All Mobile List</h2>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th>Mobile Name</th>
            <th>Model</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $sl=0;
        foreach($allResult as $result){
            $sl++?>
        <tr>
            <td><?php echo $sl?></td>
            <td><?php echo $result->id ?></td>
            <td><?php echo $result->title ?></td>
            <td><?php echo $result->model_name ?></td>
            <td> <a href="view.php" class="btn btn-info" role="button">View</a>
                <a href="edit.php" class="btn btn-info" role="button">Edit</a>
                <a href="delete.php" class="btn btn-info" role="button">Delete</a>
            </td>
        </tr>
        <?php } ?>

        </tbody>
    </table>
</div>
<script>
    $("#message").show().delay(3000).fadeOut();
</script>

</body>
</html>
